package streaming.opdracht2;

import java.util.stream.Stream;

public class App2 {
    public static void main(String[] args) {

        String[] woorden = {"Jente","is","voor","de","venten"};

        String resultaat = Stream.of(woorden).reduce(";",(acc,el)->acc+el+";");
        System.out.println(resultaat);
    }
}
