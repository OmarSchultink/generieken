package streaming.opdracht2;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.function.BinaryOperator;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class App {
    public static void main(String[] args) {


        int[] getallen = {1, 2, 10, 5, 6, 3, 8, 9 };

        System.out.println("aantal getallen in array = "+IntStream.of(getallen).count());
        System.out.println("max in array = "+IntStream.of(getallen).max());
        System.out.println("min in array = "+IntStream.of(getallen).min());
        System.out.println("gemiddelde = "+IntStream.of(getallen).average());
        System.out.println("som = "+IntStream.of(getallen).sum());

        int result = IntStream.of(getallen).reduce(0,(acc,el) -> acc+el*el);
        System.out.println("som van kwadraten is "+result);















    }
}
