package streaming.opdracht1;

import java.util.stream.Stream;

public class PersonApp {

    public static void main(String[] args) {
        Person person1 = new Person("Omar","Schultink",38,78, (float) 1.78, Person.Geslacht.M);
        Person person2 = new Person("Kris","Coolen",34,100,(float)1.70, Person.Geslacht.M);
        Person person3 = new Person("Jente","Ver Paelen",23,77,(float)1.80, Person.Geslacht.V);

        Person[] persons = {person1,person2,person3};
        Stream.of(persons).forEach(System.out::println);

    }
}
