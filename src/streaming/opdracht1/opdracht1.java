package streaming.opdracht1;

import java.util.stream.IntStream;
import java.util.stream.Stream;

public class opdracht1 {
    public static void main(String[] args) {
        String[] words = {"omar","nika","lieuwe","liana"};
        Stream<String> stream = Stream.of(words);
        stream.forEach(System.out :: println);

        IntStream iStream = IntStream.rangeClosed(0,100);
        iStream.forEach(System.out :: println);

    }

}
