package streaming.opdracht4;

import java.util.Random;
import java.util.stream.IntStream;

public class App3 {
    public static void main(String[] args) {

        System.out.println("500 random, even, uniek,tussen 0 en 10000 -> sorteren en maak een array");

        int [] randomIntsArray = IntStream.generate(()->new Random().nextInt(10000)).filter(n -> n % 2 ==0).distinct().limit(500).toArray();
        IntStream.of(randomIntsArray).sorted().forEach(System.out::println);

    }

}
