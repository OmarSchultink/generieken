package streaming.opdracht4;

import java.util.stream.IntStream;

public class App2 {
    public static void main(String[] args) {
        System.out.println("berekent de som van de vierkantwortel " +
                "van de eerste 20 getallen tussen 0 en 1000 die deelbaar zijn door 5 en 8");

        Double result = IntStream.iterate(0, i-> i+1).limit(1000).filter(i -> i % 5 ==0 && i % 8 ==0).limit(20).mapToDouble(Double::new).reduce(0,(acc,el)->acc+Math.sqrt(el));
        System.out.println(result);

    }
}
