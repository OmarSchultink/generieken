package streaming.opdracht4;


import java.util.Comparator;
import java.util.stream.Stream;

public class App {
    public static void main(String[] args) {
        Person person1 = new Person("Omar","Schultink",38,78, (float) 1.78, Person.Geslacht.M);
        Person person2 = new Person("Kris","Coolen",34,99,(float)1.70, Person.Geslacht.M);
        Person person3 = new Person("Jente","Ver Paelen",23,60,(float)1.80, Person.Geslacht.V);
        Person person4 = new Person("Nika","Schultink",11,53,150,Person.Geslacht.V);
        Person [] persons = {person1,person2,person3,person4};
        System.out.println("alle vrouwtjes");
        Stream.of(persons).filter(person -> person.getGeslacht()== Person.Geslacht.V).forEach(System.out :: println);
        System.out.println("iedereen boven 25j");
        Stream.of(persons).filter(person -> person.getLeeftijd() >25).forEach(System.out :: println);
        System.out.println("gewicht tussen 75 en 100 kg");
        Stream.of(persons).filter(person -> person.getGewicht() >75).filter(person -> person.getGewicht()<100).forEach(System.out::println);
        System.out.println("personen ouder dan 25 en zwaarder dan 90 kg");
        Stream.of(persons).filter(person -> person.getLeeftijd() >25).filter(person -> person.getGewicht()>90).forEach(System.out::println);
        System.out.println("druk enkel de leeftijden af van de personen");
        Stream.of(persons).sorted().forEach(System.out::println);
        System.out.println("Voornaam + achternaam van iedere persoon");
        Stream.of(persons).map(person -> person.getVoornaam()+" "+person.getAchternaam()).forEach(System.out::println);
        System.out.println("geef gemiddelde leeftijd, min leeftijd en max leeftijd");
        Double gemiddelde = Stream.of(persons).mapToInt(person -> person.getLeeftijd()).average().getAsDouble();
        int min = Stream.of(persons).mapToInt(person -> person.getLeeftijd()).min().getAsInt();
        int max = Stream.of(persons).mapToInt(person -> person.getLeeftijd()).max().getAsInt();
        System.out.println("gemiddelde is "+gemiddelde+" minimum is "+min+" max is "+max);
        Stream.of(persons).map(person -> person.getGewicht()).forEach(System.out ::println);
        System.out.println("geef gemiddelde gewicht, min gewicht en max gewicht");
        Double gemiddeld = Stream.of(persons).mapToDouble(person -> person.getGewicht()).average().getAsDouble();
        Double mini = Stream.of(persons).mapToDouble(person -> person.getGewicht()).min().getAsDouble();
        Double maxi = Stream.of(persons).mapToDouble(person -> person.getGewicht()).max().getAsDouble();
        System.out.println("gemiddelde is "+gemiddeld+" minimum is "+mini+" max is "+maxi);


        System.out.println("berekent de som van de vierkantwortel van de eerste 20 getallen tussen 0 en 1000 die deelbaar zijn door 5 en 8");






    }


}
