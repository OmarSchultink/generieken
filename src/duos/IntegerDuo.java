package duos;

public class IntegerDuo<E extends Comparable<E>> extends Duo<E> {
    public IntegerDuo (E first, E second){
        super(first,second);
    }
}
