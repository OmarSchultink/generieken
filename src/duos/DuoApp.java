package duos;

public class DuoApp {
    public static void main(String[] args) {
        Duo<String> sd = new Duo<>("Hello","World");
        Duo<Integer> id = new Duo<>(7,5);

        NumberDuo nd = new NumberDuo(55,77);
        IntegerDuo intDuo = new IntegerDuo(10,3);
        StringDuo stringDuo = new StringDuo("Panne","Koek");

        String s1 = sd.getFirst();
        String s2 = sd.getSecond();

        Integer i1 = id.getFirst();
        Integer i2 = id.getSecond();

        System.out.println(s1 + " " + s2);
        System.out.println(i1 + i2);

        System.out.println(sd.getHighestValue());
        System.out.println(id.getHighestValue());

        System.out.println(nd.getSum());
        System.out.println(intDuo.getHighestValue());
        System.out.println(stringDuo.getFirst());
    }
}
