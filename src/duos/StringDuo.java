package duos;

public class StringDuo<E extends Comparable<E>> extends Duo<E>{
    public StringDuo (E first, E second){
        super(first,second);
    }
}
