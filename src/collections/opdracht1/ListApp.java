package collections.opdracht1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ListApp {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("geef een getal");
        int getal1 = keyboard.nextInt();
        System.out.println("geef een getal");
        int getal2 = keyboard.nextInt();
        System.out.println("geef een getal");
        int getal3 = keyboard.nextInt();
        System.out.println("geef een getal");
        int getal4 = keyboard.nextInt();
        System.out.println("geef een getal");
        int getal5 = keyboard.nextInt();

        List<Integer> al = new ArrayList<>();
        al.add(getal1);
        al.add(getal2);
        al.add(getal3);
        al.add(getal4);
        al.add(getal5);
        System.out.println("getallen in volgorde zoals ingegeven");
        for (int i = 0; i < al.size(); i++) {
            System.out.println(al.get(i));
        }
        System.out.println("gemiddelde van de ingave");
        Double listValues = al.stream().mapToInt(Integer::valueOf).average().getAsDouble();
        System.out.println(listValues);

        System.out.println("geef de som van de ingave");
        int som = al.stream().mapToInt(Integer::valueOf).sum();
        System.out.println(som);





    }
}
