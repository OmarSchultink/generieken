package test;

import java.util.stream.IntStream;

public class TestCase {
    public static void main(String[] args) {

        int sum = IntStream.rangeClosed(0,10).sum();
        System.out.println(sum);
    }
}
