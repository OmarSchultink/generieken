package pairs;

public class Shoe {

    int size;
    String brand;
    Side side;

    public Shoe(int size,String brand, Side side){
        this.size = size;
        this.brand = brand;
        this.side = side;

    }
    public Shoe(){
        this(43,"Nike",Side.L);
    }
    public Shoe(Side side){
        this(40,"Adidas",side);
    }


    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    @Override
    public String toString() {
        return "Shoe{" +
                "size=" + size +
                ", brand='" + brand + '\'' +
                ", side=" + side +
                '}';
    }
}
