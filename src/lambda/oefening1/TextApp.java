package lambda.oefening1;

public class TextApp {

    public static void main(String[] args) {
        TextPrinter tp = new TextPrinter(
                "Hello this is an example of a sentence containing words");

        System.out.println("*** Words containing 'e' ***");
        tp.printFilteredWords(s -> s.contains("e"));

        System.out.println("*** Long words ***");
        tp.printFilteredWords(s -> s.length() > 4);

        System.out.println("*** Words starting with an 'a'");
        tp.printFilteredWords(s -> s.startsWith("a"));

        System.out.println("*** Words with second letter 'e'");
        tp.printFilteredWords(s -> {
            if (s.length() > 1) {

                if (s.charAt(1) == 'e') {
                    return true;
                } else {
                    return false;
                }
            }
            return false;
        });

        System.out.println("*** Words containing letter 'e' twice ***");
        tp.printFilteredWords(s -> {
           char [] chars = s.toCharArray();
           int count = 0;
           for (int i = 0 ; i< chars.length; i++){
               if(chars[i]=='e'){
                   count++;
               }
           }
           if (count == 2){
               return true;
           }else {return false;}
        });


    }
}

