package lambda.oefening1;

public interface WordFilter {
    public boolean isValid(String s);
}
