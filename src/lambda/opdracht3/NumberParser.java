package lambda.opdracht3;

import java.math.BigDecimal;

public interface NumberParser {
    public BigDecimal parse(String s);
}
