package lambda.opdracht3;

import java.math.BigDecimal;
import java.util.function.Function;
import java.util.function.Predicate;

public class App {
    public static void main(String[] args) {

        TextPrinter tp = new TextPrinter("Was it a car or a cat I saw", System.out::print);
        TextPrinter tp2 = new TextPrinter("appel FLAPPEN kop", System.out::print);
        TextPrinter tp3 = new TextPrinter("123 456 789", System.out::print);
        TextPrinter tp4 = new TextPrinter ("what the f is dit nu weer", System.out::print);
        TextPrinter tp5 = new TextPrinter ("ik heb het er godverdomme mee gehad, geef me een peuk", System.out::print);

        tp.printProcessedWords(TextUtil::reverse);
        /*System.out.println(TextUtil.reverse(string));*/


        tp2.printProcessedWords(new TextScrambler()::scramble);
        tp2.printProcessedWords(s -> s.toLowerCase());

        tp3.printNumberValues( BigDecimal::new);

        Predicate<String> proc1 = s -> s.contains("e");
        Predicate<String> proc2 = s -> s.contains("a");
        Predicate<String> proc3 = proc1.or(proc2);
        tp4.printFilteredWords(proc3);

        Function <String,String> pr1 = String::toUpperCase;
        Function <String,String> pr2 = TextUtil::reverse;
        tp5.printProcessedWords(pr1.andThen(pr2));









    }

}
