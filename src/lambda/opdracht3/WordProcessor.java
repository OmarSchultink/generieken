package lambda.opdracht3;

public interface WordProcessor {
    public String process(String s);
}
