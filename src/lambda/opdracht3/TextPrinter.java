package lambda.opdracht3;

import java.math.BigDecimal;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class TextPrinter {
    private String sentence;
    private Consumer <String> printer;

    public TextPrinter(String sentence, Consumer <String> printer) {
        this.sentence = sentence;
        this.printer = printer;
    }


    public void printProcessedWords(Function<String,String> processor){
        for (String w: sentence.split(" ")){
            printer.accept(processor.apply(w)+" ");
        }
        printer.accept("\n");
    }
    public void printNumberValues(Function<String, BigDecimal> parser){
        for (String w : sentence.split(" ")){
            System.out.format("%,f%n",parser.apply(w));
        }
    }
    public void printFilteredWords(Predicate<String> filter){
        for (String w: sentence.split(" ")){
            if (filter.test(w)) {
                printer.accept(w+" ");
            }
        }
    }

}
