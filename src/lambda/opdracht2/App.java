package lambda.opdracht2;

import java.math.BigDecimal;

public class App {
    public static void main(String[] args) {
        TextPrinter tp = new TextPrinter("Was it a car or a cat I saw");
        TextPrinter tp2 = new TextPrinter("appel FLAPPEN kop");
        TextPrinter tp3 = new TextPrinter("123 456 789");


        tp.printProcessedWords(TextUtil::reverse);
        /*System.out.println(TextUtil.reverse(string));*/


        tp2.printProcessedWords(new TextScrambler()::scramble);
        tp2.printProcessedWords(s -> s.toLowerCase());

        tp3.printNumberValues( BigDecimal::new);


    }

}
