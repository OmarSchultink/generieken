package lambda.opdracht2;

public class TextPrinter {
    private String sentence;

    public TextPrinter(String sentence) {
        this.sentence = sentence;
    }
    public void printProcessedWords(WordProcessor processor){
        for (String w: sentence.split(" ")){
            System.out.print(processor.process(w)+" ");
        }
        System.out.println();
    }
    public void printNumberValues(NumberParser parser){
        for (String w : sentence.split(" ")){
            System.out.format("%,f%n",parser.parse(w));
        }
    }
}
